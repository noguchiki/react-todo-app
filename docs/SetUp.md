

## nodeをインストール
[こちら](./InstallNode.md)を参照

## プロジェクトの作成
### セットアップ

```
$ npm init -y
```
### インストール

```
$ npm i -S react react-dom lodash

$ npm i -D babel-core babel-loader babel-preset-es2015 babel-preset-react react-hot-loader webpack webpack-dev-server

$ npm i -g webpack webpack-dev-server
```

### Webpackでバンドルする
`src/index.js`,`src/components/app.jsx`,`index.html`と`webpack.config.js`を追加

```
// webpack.config.js
var webpack = require('webpack');
var path = require('path');

module.exports = {
  devtool: 'inline-source-map',
  entry: [
    'webpack-dev-server/client?http://127.0.0.1:8080/',
    'webpack/hot/only-dev-server',
    './src'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  resolve: {
    modelesDirectories: ['node_modules', 'src'],
    extensions: ['', '.js']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
}
```

```
// src/index.js
import React from 'react';
import { render } from 'react-dom';
import App from './components/app.jsx'


render(<App />, document.getElementById('app'));

```

```
// src/components/app.jsx
import React from 'react';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <h1>React Todos App</h1>
      </div>
      );
  }
}
```


```
// index.html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>React Todos App</title>
</head>
<body>
  <div id="app"></div>
  <script src="bundle.js"></script>
</body>
</html>
```

### Serverの起動
プロジェクト直下で以下コマンドを実行
```
$ webpack-dev-server
```

### MaterialUIの利用
詳細は、[こちら](./UsageOfMaterialUI.md)

以上がアプリ作成にあたっての環境準備となる。
