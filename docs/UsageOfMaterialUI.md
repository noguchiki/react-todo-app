### Material UIとは
UIパーツをReactのコンポーネントとして使用できるもの。

Material UI以外のReactのコンポーネントセット
詳細は、[こちら](http://qiita.com/kyrieleison/items/39ce30dd2d204791a9ea)

### Material UIを使う準備
#### インストール
まず、Material UIをインストール。Material UIでは、`react-tap-event-plugin`というモジュールが使われているので、あわせてインストールが必要。
```
npm i -S material-ui react-tap-event-plugin
```
これにより、`package.json`にも反映。
#### 使う前の準備
まず、htmlファイルにレンダリングしている`index.js`にて、
```
import injectTapEventPlugin from "react-tap-event-plugin";

injectTapEventPlugin();
```
を追記。

実際のコード↓

```
// index.js
import React from 'react';
import { render } from 'react-dom';
import App from './components/app.jsx'
import injectTapEventPlugin from "react-tap-event-plugin";

injectTapEventPlugin();

render(<App />, document.getElementById('app'));

```

`app.jsx`にて、以下を追加
```
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
```
その上で、`MuiThemeProvider`でコンポーネントを囲う。  
使用例↓
```
render() {
  return (
    <MuiThemeProvider>
      <div>
        <CreateTodo />
        <TodosList />
      </div>
    </MuiThemeProvider>
  )
}
```

#### 実際に使用する！
以上で準備は完了。ボタンを作ってみる。

```
import RaisedButton from 'material-ui/RaisedButton';
```
↑のように、インポートをしてあげ、
そのうえで、他のコンポーネントと同じように記述↓

```
  <RaisedButton label="Create" />
```
