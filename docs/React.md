React
---
## Reactとは
React.jsはFacebookが作成したJavaScriptのビュー・ライブラリ。
コンポーネントの状態がReactによって管理され、データに変更があるときには更新の必要があるコンポーネントだけ更新される。そのため、効率的にレンダリングさせることが可能。

## メリット
上記の説明でも触れたとおり、必要に応じて、効率的なレンダリングが可能。
(Virtual DOMで、変更分（差分）のみDOMに反映されるため)


## デメリット
導入するコスト（開発を行う準備）が高く、始めずらい。

## 学んだこと

### コンポーネントについて
 Reactはコンポーネントを組み合わせることでアプリケーションを作成することができる。

 たとえば、テーブルを作る際には、以下のような構造が想定できる

 - app.jsx
    - Table.jsx
        - TableHeader.jsx
        - TableBody.jsx

このようにコンポーネント間に親子関係が生まれる。
その際に、子コンポーネントを親コンポーネントから参照する方法は↓

```
import React from 'react';
import TableHeader from 'TableHeader.jsx';
import TableBody from 'TableBody.jsx';

export default class Table extends React.Component {

  render() {
    return (
        <table>
          <TableHeader />
          <TableBody />
        <table />
      )
  }
}

```
まず、参照した子コンポーネントを`import`する。
そうすることで、子コンポーネントをタグのように使用することができる。


### stateとprops
- stateとpropsの違い
 - state  
 そのコンポーネントが持っている状態。
 変更可能。
 - props  
 親コンポーネントから渡されたプロパティ。
 変更不可。

- コンポーネント間でのプロパティの受け渡し

```
import React from 'react';
import Login from 'Login.jsx'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedin: false,
    };
  }

  isLoggedin() {
    this.setState({showResult: false});
  }

  render() {
    const top = (
      <div>
        ログインしました。
      </div>
    );

    const login = (
      <div>
        <Login
          isLoggedin={this.isLoggedin.bind(this)}
        />
      </div>
    );

    return (
      <MuiThemeProvider>
        <div style={style.container}>
          <h1ログインしてください</h1>
          {this.state.isLoggedin ? top : login }
        </div>
      </MuiThemeProvider>
    )
  }
}

```
親である`app.jsx`にて、`isLoggedin`関数を定義。

その関数をプロパティとして`Login.jsx`に値渡しをしています。↓
```
<Login
  isLoggedin={this.isLoggedin.bind(this)}
/>
```
これにより子コンポーネントのイベントにて、isLoggedin関数を実行することが可能になります。


```
// Login.jsx
import React from 'react';
export default class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div>
          <FlatButton>
            label="login"
            onClick={this.props.isLoggedin}
          />
        </div>
    )
  }
}
```

子コンポーネントでは、`this.props.[プロパティ名]`で呼び出しを行うことが可能です。


### 参考
http://qiita.com/kyrieleison/items/78b3295ff3f37969ab50
