### anyenvのインストール
```
$ git clone https://github.com/riywo/anyenv ~/.anyenv

```

### PATHの設定
```
$ echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> ~/.your_profile
$ echo 'eval "$(anyenv init -)"' >> ~/.your_profile
```
your_profile のところは、利用しているシェルの設定ファイルを指定  
私の場合は、`bash_profile`

最後にシェルの再起動。
```
exec $SHELL -l
```

以上でanyenvのインストールは完了

### Nodeをインストール
**envでいろいろインストール可能。今回はNodeなので`ndenv`。
```
$ anyenv install ndenv
```

これがインストールできたら、ndenvからNodeをインストール

```
$ ndenv install v6.2.0
```

これで、完了

  
