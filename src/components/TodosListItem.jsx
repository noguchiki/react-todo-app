import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';


const style = {
  buttonAlignStyle: {
    margin: 10,
  },
  icon: {
    margin: 0,
  },
};

export default class TodosListItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
    };
  }

  onEditClick() {
    this.setState({ isEditing: true});
  }

  onCancelClick() {
    this.setState({ isEditing: false});
  }

  onSaveClick() {
    event.preventDefault();

    const oldTask = this.props.task;
    const newTask = this.state.newTaskValue;
    this.props.saveTask(oldTask, newTask);
    this.setState({ isEditing: false });
  }

  renderTaskSection() {
    const { task, isCompleted } = this.props;
    const taskStyle = {
      color: isCompleted ? 'green' : 'red',
      cursor: 'pointer'
    };

    if(this.state.isEditing) {
      return (
        <TableRowColumn>
          <form onSubmit={this.onSaveClick.bind(this)}>
            <TextField id="task-edit-controlled" type="text" defaultValue={task} onChange={e => this.setState({ newTaskValue: e.target.value})} />
          </form>
        </TableRowColumn>
      )
    }

    return (
      <TableRowColumn style={taskStyle} disabled={isCompleted}
        >
        {task}
      </TableRowColumn>
    );
  }


  renderActionSection() {
    if(this.state.isEditing) {
      return (
        <TableRowColumn>
          <RaisedButton disabled={this.props.isCompleted} label="Save" onClick={this.onSaveClick.bind(this)} />
          <RaisedButton disabled={this.props.isCompleted} label="Cancel" style={style.buttonAlignStyle} onClick={this.onCancelClick.bind(this)} />
        </TableRowColumn>
      );
    }

    return (
      <TableRowColumn>
        <RaisedButton disabled={this.props.isCompleted} label="Edit" onClick={this.onEditClick.bind(this)} />
        <RaisedButton disabled={this.props.isCompleted} label="Delete" style={style.buttonAlignStyle} onClick={this.props.deleteTask.bind(this, this.props.task)} />
      </TableRowColumn>
    );
  }

  render() {
    return (
      <TableRow>
        <TableRowColumn>
          <Checkbox defaultChecked={this.props.isCompleted} onCheck={this.props.toggleTask.bind(this, this.props.task)} disabled={this.props.isCompleted}/>
        </TableRowColumn>
        {this.renderTaskSection()}
        {this.renderActionSection()}
      </TableRow>
    )
  }
}
