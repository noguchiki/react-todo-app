import React from 'react';
import TodosList from './TodosList.jsx'
import CreateTodo from './CreateTodo.jsx'
import TodosDoneList from './TodosDoneList.jsx'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const todos = [];

const style = {
  container: {
    textAlign: 'center',
    paddingTop: 100,
  },
};


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todos,
      showResult: false,
    };
  }

  toggleTask(task) {
    console.log("isCalledToggleTask");
    const foundTodo = _.find(this.state.todos, todo => todo.task === task);
    foundTodo.isCompleted = !foundTodo.isCompleted;
    this.setState({ todos: this.state.todos });
  }

  createTask(task) {
    this.state.todos.push({
      task,
      isCompleted: false
    });
    this.setState({ todos: this.state.todos });
  }

  saveTask(oldTask, newTask) {
    const foundTodo = _.find(this.state.todos, todo => todo.task === oldTask);
    foundTodo.task = newTask;
    this.setState({ todos: this.state.todos });
  }

  deleteTask(taskToDelete) {
    _.remove(this.state.todos, todo => todo.task === taskToDelete);
    this.setState({ todos: this.state.todos });
  }

  showResult() {
    this.setState({showResult: true});
  }

  showTodosList() {
    this.setState({showResult: false});
  }

  render() {
    const list = (
      <div>
        <CreateTodo
          todos={this.state.todos}
          createTask={this.createTask.bind(this)} />
        <TodosList
          todos={this.state.todos}
          toggleTask={this.toggleTask.bind(this)}
          saveTask={this.saveTask.bind(this)}
          deleteTask={this.deleteTask.bind(this)}
          showResult={this.showResult.bind(this)}
          />
      </div>
    );

    const result = (
      <div>
        <div>成果です</div>
        <TodosDoneList
          todos={this.state.todos.filter((todo) => todo.isCompleted)}
          showTodosList={this.showTodosList.bind(this)}
        />
      </div>
    );

    return (
      <MuiThemeProvider>
        <div style={style.container}>
          <h1>React Todos App</h1>
          {this.state.showResult ? result : list }
        </div>
      </MuiThemeProvider>
    )
  }
}
