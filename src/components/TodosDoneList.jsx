import _ from 'lodash';
import React from 'react';
import TodosListItem from './TodosListItem.jsx';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

const style = {
  table: {
    width: '50%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  footerButton: {
    margin: 15,
  },
};

export default class TodosDoneList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showCheckboxes: false,
      isClickedResult: true,
    };
  }

  render() {
    return (
      <div>
        <Table style={style.table}>
          <TableHeader displaySelectAll={this.state.showCheckboxes} adjustForCheckbox={this.state.showCheckboxes}>
            <TableRow>
              <TableHeaderColumn>Task</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody  displayRowCheckbox={this.state.showCheckboxes}>
            {this.props.todos.map((a, index) =>
              <TableRow key={index}>
                <TableRowColumn>
                  {a.task}
                </TableRowColumn>
              </TableRow>
            )}
        </TableBody>
        </Table>
        <FlatButton
          style={style.footerButton}
          label="Back to TodosList"
          onClick={this.props.showTodosList}
        />
      </div>
    )
  }
}
