import _ from 'lodash';
import React from 'react';
import TodosListItem from './TodosListItem.jsx';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow } from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

const style = {
  table: {
    width: '50%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  footerButton: {
    margin: 15,
  },
};

export default class TodosList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showCheckboxes: false,
      isClickedResult: true,
    };
  }

  renderItems() {
    const props = _.omit(this.props, 'todos');
    return _.map(this.props.todos, (todo, index) => <TodosListItem key={index
    } {...todo} {...props}/>);
  }

  render() {
    return (
      <div>
        <Table style={style.table}>
          <TableHeader displaySelectAll={this.state.showCheckboxes} adjustForCheckbox={this.state.showCheckboxes}>
            <TableRow>
              <TableHeaderColumn>Status</TableHeaderColumn>
              <TableHeaderColumn>Task</TableHeaderColumn>
              <TableHeaderColumn>Action</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={this.state.showCheckboxes}>
            {this.renderItems()}
          </TableBody>
        </Table>
        <FlatButton
          style={style.footerButton}
          label="今日の成果は..."
          onClick={this.props.showResult}
        />
      </div>
    )
  }
}
