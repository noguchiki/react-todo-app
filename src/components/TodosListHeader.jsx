import React from 'react';
import { TableHeader, TableRow, TableHeaderColumn } from 'material-ui/Table';

export default class TodosListHeader extends React.Component {

  render() {
    return (
      <TableHeader>
        <TableRow>
          <TableHeaderColumn> {this.sub}</TableHeaderColumn>
          <TableHeaderColumn>Action</TableHeaderColumn>
        </TableRow>
      </TableHeader>
    )
  }
}
