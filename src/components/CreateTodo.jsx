import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


export default class TodosList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      error: null
    };
  }

  renderError() {
    if (!this.state.error) {
      return null;
    }

    return <div style={{ color: 'red'}}>{this.state.error}</div>;
  }

  render() {
    return (
      <form onSubmit={this.handleCreate.bind(this)}>
        <TextField
          id="text-field-controlled"
          placeholder="What do I need to do?"
          onChange={e => this.setState({ textFieldValue: e.target.value})}
          />
        <RaisedButton
          type="submit"
          label="Create"
          primary={true}
          />
        {this.renderError()}
      </form>
    )
  }

  handleCreate(event) {
    event.preventDefault();

    // const createInput = this.refs.createInput;
    // const task = createInput.value;
    const task = this.state.textFieldValue;
    console.log(this.state);

    const validateInput = this.validateInput(task);

    if (validateInput) {
      this.setState({ error: validateInput});
      return;
    }

    this.setState({ error: null });
    this.props.createTask(task);
    // this.refs.createInput.value = '';
    this.setState({
      textFieldValue: '',
    });
    const text = document.getElementById('text-field-controlled');
    text.value = '';
  }

  validateInput(task) {
    if (!task) {
      return 'Please enter a task.';
    } else if (_.find(this.props.todos, todo => todo.task === task)) {
      return 'Task Already exisit.';
    } else {
      return null;
    }
  }
}
